import { createGuiFromDescription } from './gui.js'

let parameters = {}
let controllers = {}

let parametersDescription = {
    radius: {
        value: 25,
        min: 1,
        max: 100,
    },
    width: {
        value: 130,
        min: 0,
        max: 200,
        step: 1
    },
    bridgeWidth: {
        value: 10,
        min: 2,
        max: 40
    },
    bridgeHeight: {
        value: 10,
        min: 2,
        max: 40
    },
    bridgeOffset: {
        value: 5,
        min: 1,
        max: 40
    },
    branchLength: {
        value: 100,
        min: 10,
        max: 200,
    },
    branchLength2: {
        value: 20,
        min: 0,
        max: 50,
    },
    branchThickness: {
        value: 10,
        min: 2,
        max: 40
    },
    branchAngle: {
        value: 45,
        min: 0,
        max: 90
    },
    branchRadius: {
        value: 11,
        min: 0,
        max: 40
    },
    thickness: {
        value: 10,
        min: 0,
        max: 40
    },
    notchHeight: {
        value: 5,
        min: 0,
        max: 40
    },
    notchHeight: {
        value: 5,
        min: 0,
        max: 40
    },
    woodThickness: {
        value: 3,
        min: 0,
        max: 15,
        step: 1
    },
    margin: {
        value: 20,
        min: 0,
        max: 40
    },
}

let style = {
    strokeColor: 'black',
    strokeWidth: 1
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

export function generate() {

    paper.project.activeLayer.removeChildren()

    let bridgeWidth = parameters.bridgeWidth
    let bridgeHeight = parameters.bridgeHeight
    let bridgeOffset = parameters.bridgeOffset
    let radius = parameters.radius
    let width = parameters.width
    
    let branchLength = parameters.branchLength
    let branchLength2 = parameters.branchLength2

    let branchThickness = parameters.branchThickness
    let branchAngle = parameters.branchAngle
    let branchRadius = parameters.branchRadius
    let thickness = parameters.thickness
    let margin = parameters.margin
    let notchHeight = parameters.notchHeight
    let woodThickness = parameters.woodThickness

    let position = paper.view.bounds.topLeft.add(margin + width, margin + radius)

    let leftGlassOut = new paper.Path.Circle(position.subtract(bridgeWidth / 2 + radius, 0), radius)
    let leftGlassIn = new paper.Path.Circle(position.subtract(bridgeWidth / 2 + radius, 0), radius - thickness)
    let rightGlassOut = new paper.Path.Circle(position.add(bridgeWidth / 2 + radius, 0), radius)
    let rightGlassIn = new paper.Path.Circle(position.add(bridgeWidth / 2 + radius, 0), radius - thickness)

    let brigeMid = new paper.Path.Rectangle(position.subtract(bridgeWidth/2-radius, bridgeHeight/2), position.add(bridgeWidth/2-radius, bridgeHeight/2))
    
    let bridgeLeft = new paper.Path.Rectangle(position.subtract(width/2, bridgeHeight/2), position.subtract(bridgeWidth/2+radius, -bridgeHeight/2))
    let bridgeLeftNotch = new paper.Path.Rectangle(position.subtract(width/2+woodThickness, notchHeight/2), position.subtract(width/2-woodThickness, -notchHeight/2))
    
    let bridgeRight = new paper.Path.Rectangle(position.add(bridgeWidth/2 + radius, -bridgeHeight/2), position.add(width/2, bridgeHeight/2))
    let bridgeRightNotch = new paper.Path.Rectangle(position.add(width/2-woodThickness, -notchHeight/2), position.add(width/2+woodThickness, notchHeight/2))
    
    let glasses = leftGlassOut.unite(brigeMid)
    glasses = glasses.unite(rightGlassOut)
    glasses = glasses.unite(bridgeLeft.subtract(bridgeLeftNotch))
    glasses = glasses.unite(bridgeRight.subtract(bridgeRightNotch))
    glasses = glasses.subtract(rightGlassIn)
    glasses = glasses.subtract(leftGlassIn)

    glasses.style = style

    position.x = paper.view.bounds.left + margin + woodThickness + 1.5 * branchLength2
    position.y += radius + margin

    let branch = new paper.Path()
    let p1 = position.clone()
    let delta = new paper.Point(1, 0)
    delta.angle += branchAngle
    let delta2 = delta.clone()
    delta2.angle += 90

    let x = Math.tan(2 * Math.PI * (branchAngle/2) / 360) * (branchThickness / 2)

    branch.closed = true
    branch.add(p1.add(0, -branchThickness / 2))
    branch.add(p1.add(branchLength + x, -branchThickness / 2))
    branch.add(branch.lastSegment.point.add(delta.multiply(branchLength2)))
    branch.add(branch.lastSegment.point.add(delta2.multiply(branchThickness)))
    branch.add(p1.add(branchLength - x, branchThickness / 2))
    branch.add(p1.add(0, branchThickness / 2))
    branch.add(branch.firstSegment.point)

    let branchNotch = new paper.Path.Rectangle(position.subtract(woodThickness, notchHeight/2), position.add(0, notchHeight/2))
    branch = branch.unite(branchNotch)

    if(branchRadius > branchThickness / 2) {
        let branchTip = p1.add(branchLength, 0)
        branchTip = branchTip.add(delta.multiply(branchLength2))
        let branchCircle = new paper.Path.Circle(branchTip, branchRadius)
        branch = branch.unite(branchCircle)
    }
    branch.style = style
    let secondBranch = branch.clone()
    secondBranch.position.x -= 1.5 * branchLength2
    secondBranch.position.y += branchThickness + branchRadius

    secondBranch.scale(-1, 1)

}
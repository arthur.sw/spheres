/*

// Example usage

let parameters = {}
let controllers = {}

let parametersDescription = {
	type: {
		value: oscillator.type,
		options: ['sine', 'square', 'triangle', 'sawtooth', 'pwm', 'pulse'],
		onChange: (value)=> {
			oscillator.type = value
			controllers['modulation'].setValue('none')
			console.log(oscillator.type)
		},
		reset: true
	},
	frequency: {
		value: oscillator.frequency ? oscillator.frequency.value : 440,
		min: 1,
		max: 4,
		log: true,
	},
	detune: {
		value: oscillator.detune ? oscillator.detune.value : 0,
		min: 0.01,
		max: 1000,
	},
	modulation: {
		value: 'none',
		options: ['none', 'am', 'fm', 'fat'],
		onChange: (value)=> {
			let primitiveType = '' + oscillator.type
			primitiveType = primitiveType.replace('am', '')
			primitiveType = primitiveType.replace('fm', '')
			primitiveType = primitiveType.replace('fat', '')
			if(value == 'none') {
				oscillator.type = primitiveType
			} else {
				oscillator.type = value + primitiveType
			}
			console.log(oscillator.type)
		},
		reset: true,
	},
	modulationType: {
		value: oscillator.modulationType ? oscillator.modulationType : 'none',
		options: ['sine', 'square', 'triangle', 'sawtooth', 'pwm', 'pulse'],
		reset: ()=> {
			if(oscillator.count) {
				oscillator.count = parameters.count
			}
			if(oscillator.detune) {
				oscillator.detune.value = parameters.detune
			}
			if(oscillator.harmonicity) {
				oscillator.harmonicity.value = parameters.harmonicity
			}	
			if(oscillator.modulationFrequency) {
				oscillator.modulationFrequency.value = parameters.modulationFrequency
			}
			if(oscillator.partials) {
				// oscillator.partials = parameters.partials
			}
			if(oscillator.phase) {
				oscillator.phase = parameters.phase
			}
			if(oscillator.spread) {
				oscillator.spread = parameters.spread
			}
			if(oscillator.width) {
				oscillator.width.value = parameters.width
			}
		},
	},
	harmonicity: {
		value: oscillator.harmonicity ? oscillator.harmonicity.value : 0,
		min: -4,
		max: 4,
		log: true,
	},
	modulationIndex: {
		value: oscillator.modulationIndex,
		min: 0,
		max: 10,
	},
	modulationFrequency: {
		value: oscillator.modulationFrequency ? oscillator.modulationFrequency.value : 1,
		min: 1,
		max: 5,
		log: true,
		name: 'PWM freq.'
	},


	phase: {
		value: oscillator.phase,
		min: 0,
		max: 360,
	},
	count: {
		value: oscillator.count,
		min: 1,
		max: 10,
	},
	spread: {
		value: oscillator.spread,
		min: 0.1,
		max: 1000,
	},
	partials: {
		value: '[]',
		onChange: ()=> {},
		onFinishChange: (value)=> {
			let json = JSON.parse(value)
			oscillator.partials = json
			parameters.partials = json
			document.activeElement.blur()
		}
	},
	width: {
		value: oscillator.width ? oscillator.width.value : 0,
		min: 0,
		max: 1,
		name: 'Pulse width'
	}
}
*/

export function createGuiFromDescription(folder, parametersDescription, parameters, controllers = {}, onFinishChangeFunction) {

	for(let name in parametersDescription) {
		parameters[name] = parametersDescription[name].value ? parametersDescription[name].value : 0
		
		let parameter1 = parametersDescription[name].options ? parametersDescription[name].options : parametersDescription[name].min
		let parameter2 = parametersDescription[name].max
		let parameter3 = parametersDescription[name].step ? parametersDescription[name].step : 0.1
		let log = parametersDescription[name].log
		let reset = parametersDescription[name].reset
		let parameterName = parametersDescription[name].name ? parametersDescription[name].name : name

		let onChange = parametersDescription[name].onChange ? parametersDescription[name].onChange : ()=> { return null }

		let onFinishChange = parametersDescription[name].onFinishChange ? parametersDescription[name].onFinishChange : onFinishChangeFunction
		controllers[name] = folder.add(parameters, name, parameter1, parameter2, parameter3).onChange(onChange).onFinishChange(onFinishChange).name(parameterName)
	}

}
import { createGuiFromDescription } from './gui.js'
import { generateLine } from './icosahedron.js'

let parameters = {}
let controllers = {}

let parametersDescription = {
    size: {
        value: 100,
        min: 1,
        max: 300,
    },
    nIterations: {
        value: 3,
        min: 0,
        max: 4,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

function generateCube(position, size, clockwise, n, style, cube) {
    if(clockwise) {
        generateLine(position, position.add(size/2, 0), style, cube)
        generateLine(position.add(size, 0), position.add(size, size/2), style, cube)
        generateLine(position.add(size, size), position.add(size/2, size), style, cube)
        generateLine(position.add(0, size), position.add(0, size/2), style, cube)
    } else {
        generateLine(position.add(size/2, 0), position.add(size, 0), style, cube)
        generateLine(position.add(size, size/2), position.add(size, size), style, cube)
        generateLine(position.add(size/2, size), position.add(0, size), style, cube)
        generateLine(position.add(0, size/2), position, style, cube)
    }

    if( n > 0 ) {
        generateCube(position, size/2, true, n-1, style, cube)
        generateCube(position.add(size/2, 0), size/2, false, n-1, style, cube)
        generateCube(position.add(size/2, size/2), size/2, true, n-1, style, cube)
        generateCube(position.add(0, size/2), size/2, false, n-1, style, cube)
    }
}

export function generate() {
    paper.project.activeLayer.removeChildren()
    let cube = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    // generateCube(position, size, true, 2, style, cube)

    for(let i=0 ; i<3 ; i++) {
        let position = new paper.Point(i*size, i==1 ? 0 : size)
        for(let j=0 ; j< (i==1 ? 4 : 1) ; j++) {
            generateCube(position, size, j%2==0, parameters.nIterations, style, cube)

            position.y += size
        }
    }

    let p0 = new paper.Point(0, 0)
    let p = new paper.Path()
    p.style = style
    cube.addChild(p)
    p.add(p0.add(size, 0))
    p.add(p0.add(2*size, 0))
    p.add(p0.add(2*size, size))
    p.add(p0.add(3*size, size))
    p.add(p0.add(3*size, 2*size))
    p.add(p0.add(2*size, 2*size))
    p.add(p0.add(2*size, 4*size))
    p.add(p0.add(size, 4*size))
    p.add(p0.add(size, 2*size))
    p.add(p0.add(0, 2*size))
    p.add(p0.add(0, size))
    p.add(p0.add(size, size))
    p.add(p0.add(size, 0))

    cube.position = paper.view.bounds.center
}

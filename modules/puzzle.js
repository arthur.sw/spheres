import { createGuiFromDescription } from './gui.js'
import { generateLine } from './icosahedron.js'

let parameters = {}
let controllers = {}

const margin = 2

let parametersDescription = {
    size: {
        value: 200,
        min: 1,
        max: 250,
    },
    stepLength: {
        value: 50,
        min: 1,
        max: 100,
    },
    stepRadius: {
        value: 25,
        min: 0,
        max: 50,
    },
    notchPosition: {
        value: 10,
        min: 0,
        max: 20,
    },
    notchSize: {
        value: 5,
        min: 0,
        max: 20,
    }, 
    coreSize: {
        value: 3,
        min: 0,
        max: 20,
    },
    woodThickness: {
        value: 3,
        min: 0,
        max: 10,
    }, 
    connectorHeight: {
        value: 3,
        min: 0,
        max: 10,
    },    
    connectorBottom: {
        value: 3,
        min: 0,
        max: 10,
    },
    roundedConnector: {
        value: 0,
        min: 0,
        max: 5
    },
    cutHeight1: {
        value: 10,
        min: 0,
        max: 100
    },
    cutHeight2: {
        value: 10,
        min: 0,
        max: 100
    },
    socketRadius: {
        value: 5,
        min: 0,
        max: 100
    },
    openning: {
        value: 10,
        min: 0,
        max: 100
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

let optimalBeizerCircleDistance = 4 * (Math.sqrt(2) - 1) / 3
let icosahedronDihedralAngle = Math.PI - Math.acos(Math.sqrt(5) / 3)
icosahedronDihedralAngle = 360 * icosahedronDihedralAngle / (2 * Math.PI)

// function roundPath(path, segment, amountBefore, amounAfter) {
//     let before = path.getPointAt(segment.location.offset-amountBefore)
//     let after = path.getPointAt(segment.location.offset+amountAfter)

//     let sb = p.add(before)
//     sb.handleOut = segment.point.subtract(before).multiply(optimalBeizerCircleDistance)
//     let sa = p.add(after)
//     sa.handleIn = segment.point.subtract(after).multiply(optimalBeizerCircleDistance)


// }

function roundedCorner(path, cornerRadius) {
    if(cornerRadius<=0) {
        return
    }
    let p = new paper.Path()
    for(let segment of path.segments) {
        if(segment == path.firstSegment && !path.closed) {
            p.add(segment)
        } else if(segment != path.firstSegment && segment != path.lastSegment) {
            let before = path.getPointAt(segment.location.offset-cornerRadius)
            let after = path.getPointAt(segment.location.offset+cornerRadius)
            // let c = new paper.Path.Circle(before, 2)
            // c.fillColor = 'red'
            // p.add(path.getPointAt(segment.location.offset-cornerRadius-0.1))
            // p.arcTo(before, after)

            let sb = p.add(before)
            sb.handleOut = segment.point.subtract(before).multiply(optimalBeizerCircleDistance)
            let sa = p.add(after)
            sa.handleIn = segment.point.subtract(after).multiply(optimalBeizerCircleDistance)

            // c = new paper.Path.Circle(after, 2)
            // c.fillColor = 'red'
        }
    }
    if(!path.closed) {
        p.add(path.lastSegment)
    }
    p.style = path.style
    p.position = path.position
    path.parent.addChild(p)
    path.remove()
    return p
}

function generatePuzzle(a, b, size, style, icosahedron) {
    let aToB = b.subtract(a)
    let length = aToB.getLength()
    aToB = aToB.normalize()
    let ortho = aToB.clone()
    ortho.angle = ortho.angle + 90

    let a01 = a.add(aToB.multiply(parameters.notchPosition))
    let a02 = a01.add(ortho.multiply(-parameters.notchSize / 2))
    let a03 = a02.add(aToB.multiply(parameters.woodThickness))
    let a04 = a03.add(ortho.multiply(parameters.notchSize / 2))

    let a1 = a.add(aToB.multiply(length / 2 - size))
    let a2 = a1.add(ortho.multiply(size))
    let a3 = a2.add(aToB.multiply(size))
    let a4 = a3.subtract(ortho.multiply(2 * size))
    let a5 = a4.add(aToB.multiply(size))
    let a6 = a5.add(ortho.multiply(size))

    let a61 = a.add(aToB.multiply(length - parameters.notchPosition))
    let a62 = a61.add(ortho.multiply(-parameters.notchSize / 2))
    let a63 = a62.add(aToB.multiply(-parameters.woodThickness))
    let a64 = a63.add(ortho.multiply(parameters.notchSize / 2))

    let p1 = generateLine(a01, a02, style, icosahedron)
    p1.add(a03)
    p1.add(a04)

    let p = generateLine(a, a1, style, icosahedron)
    p.add(a2)
    p.add(a3)
    p.add(a4)
    p.add(a5)
    p.add(a6)
    p.add(b)

    let p2 = generateLine(a61, a62, style, icosahedron)
    p2.add(a63)
    p2.add(a64)

    p = roundedCorner(p, parameters.stepRadius)
    // p.strokeWidth = 10
    // p.strokeColor = 'blue'
    // p.opacity = 0.3

    return p
}

function symmetry(path, axeOrigin, axeDirection) {
    let points = []
    for(let segment of path.segments) {
        let projection = axeOrigin.add(axeDirection.multiply(segment.point.subtract(axeOrigin).dot(axeDirection)))
        let direction = projection.subtract(segment.point)
        points.push(segment.point.add(direction.multiply(2)))
    }
    for(let point of points) {
        path.insert(0, point)
    }
    path.closed = true
}

function generateConnector(position, notchSize, coreSize, woodThickness, height, style) {
    let p = new paper.Path()
    p.style = style
    p.add(position)
    p.add(position.add((notchSize / 2) + coreSize, 0))
    p.add(p.lastSegment.point.add(0, height))
    p.add(p.lastSegment.point.add(-notchSize / 2, 0))
    p.add(p.lastSegment.point.add(0, woodThickness))
    p.add(p.lastSegment.point.add(notchSize / 2, 0))
    p.add(p.lastSegment.point.add(0, height))
    let xInv = new paper.Point(1, 0)
    xInv.angle += icosahedronDihedralAngle / 2
    symmetry(p, p.firstSegment.point, xInv)

    if(parameters.roundedConnector > 0) {

        let handleOut = p.segments[5].point.subtract(p.segments[4].point).multiply(parameters.roundedConnector)

        let handleIn = p.segments[8].point.subtract(p.segments[9].point).multiply(parameters.roundedConnector)
        
        p.removeSegment(5)
        p.removeSegment(5)
        p.removeSegment(5)
        p.removeSegment(5)
    
        p.segments[4].handleOut = handleOut
        p.segments[5].handleIn = handleIn
    
    }
    return p
}

let sqrt3o2 = Math.sqrt(3) / 2

export function generate() {
    paper.project.activeLayer.removeChildren()
    let icosahedron = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    let position = new paper.Point(size / 2, 0)

    let connectorTotalHeight = 2 * parameters.connectorHeight + parameters.woodThickness
    
    let h = new paper.Point(connectorTotalHeight, 0)
    h.angle = icosahedronDihedralAngle / 2
    let coreLength = parameters.coreSize - h.dot(new paper.Point(1, 0)) / 2.0

    let offset = 2 * coreLength * Math.cos(Math.PI / 3)
    let d0 = new paper.Point(offset, 0)
    d0.angle = 90
    let d1 = new paper.Point(offset, 0)
    d1.angle = - 180 / 6
    let d2 = new paper.Point(offset, 0)
    d2.angle = - 180 + 180 / 6

    let p0 = position.add(d0)
    let p1 = position.add(-size / 2, sqrt3o2 * size).add(d1)
    let p2 = position.add(size / 2, sqrt3o2 * size).add(d2)
    
    let triangleEdge1 = generatePuzzle(p0, p1, parameters.stepLength, style, icosahedron)
    let triangleEdge2 = generatePuzzle(p1, p2, parameters.stepLength, style, icosahedron)
    let triangleEdge3 = generatePuzzle(p2, p0, parameters.stepLength, style, icosahedron)

    p0 = position.add(size / 2, sqrt3o2 * size).subtract(d0)
    p1 = position.subtract(d2)
    p2 = position.add(size, 0).subtract(d1)
    
    let triangleEdge4 = generatePuzzle(p0, p2, parameters.stepLength, style, icosahedron)
    let triangleEdge5 = generatePuzzle(p2, p1, parameters.stepLength, style, icosahedron)
    let triangleEdge6 = generatePuzzle(p1, p0, parameters.stepLength, style, icosahedron)
    
    // p0 = position.add(0, 0)
    // p1 = position.add(size / 2, sqrt3o2 * size)
    // p2 = position.add(-size / 2, sqrt3o2 * size)
    // let triangle = new paper.Path()
    // triangle.add(p0)
    // triangle.add(p1)
    // triangle.add(p2)
    // triangle.add(p0)
    // triangle.strokeWidth = 1
    // triangle.strokeColor = 'black'
    // icosahedron.addChild(triangle)
    
    // console.log(icosahedron.position, triangleEdge1.firstSegment.point)
    let icosahedronDefinition = new paper.SymbolDefinition(icosahedron)
    // console.log(icosahedron.position, triangleEdge1.firstSegment.point)

    // Math.cos(Math.PI / 6) = parameters.cutHeight / x
    // x = parameters.cutHeight / Math.cos(Math.PI / 6)

    
    let pieces = new paper.Group()
    let cuts = new paper.Group()
    pieces.addChild(cuts)
    let cutOffset = new paper.Point()

    let pentagonGroup = new paper.Group()
    for(let i=0 ; i<2 ; i++) {

        let pentagonRadius = (i==0 ? parameters.cutHeight1 : parameters.cutHeight2) / Math.cos(Math.PI / 6)
        let pentagonPosition = new paper.Point(pentagonGroup.bounds.width + pentagonRadius, 0)
        let pentagon = new paper.Path.RegularPolygon(pentagonPosition, 5, pentagonRadius)
        pentagon.style = triangleEdge1.style
        let pentagonCircle = new paper.Path.Circle(pentagonPosition, i==0 ? parameters.socketRadius : parameters.openning)
        pentagonCircle.style = pentagon.style
        
        pentagonGroup.addChild(pentagon)
        pentagonGroup.addChild(pentagonCircle)

        let cut = new paper.Path()
        cut.style = triangleEdge1.style
        // cut.add(new paper.Point({ length: parameters.cutHeight, angle: 2 * 180 / 3 }))
        // cut.add(new paper.Point({ length: parameters.cutHeight, angle: 180 / 3 }))
    
        cut.add(i==0 ? triangleEdge1.firstSegment.point.add(-size, parameters.cutHeight1 - d0.y) : triangleEdge4.firstSegment.point.add(-size, - parameters.cutHeight2 + d0.y) )
        cut.add(i==0 ? triangleEdge1.firstSegment.point.add(size, parameters.cutHeight1 - d0.y) : triangleEdge4.firstSegment.point.add(+size, - parameters.cutHeight2 + d0.y) )
    
        let crossings1 = i==0 ? triangleEdge1.getCrossings(cut) : triangleEdge6.getCrossings(cut)
        let crossings2 = i==0 ? triangleEdge3.getCrossings(cut) : triangleEdge4.getCrossings(cut)
        crossings1.sort((a, b)=> a.point.x - b.point.x)
        crossings2.sort((a, b)=> a.point.x - b.point.x)
        
        
    
    
        // pieces.addChild(icosahedron)
        pieces.addChild(cut)
    
        if(crossings1.length > 0 && crossings2.length > 0) {
            let cut1 = new paper.Path()
            cut1.add(crossings1[crossings1.length-1].point)
            cut1.add(crossings2[0].point)
            cut1.style = cut.style
            cuts.addChild(cut1)
            cut.remove()
    
            if(crossings1.length == 3) {
                let cut2 = new paper.Path()
                cut2.add(crossings1[0].point)
                cut2.add(crossings1[1].point)
                cut2.style = cut1.style
                cuts.addChild(cut2)
            }
            if(crossings2.length == 3) {
                let cut3 = new paper.Path()
                cut3.add(crossings2[1].point)
                cut3.add(crossings2[2].point)
                cut3.style = cut1.style
                cuts.addChild(cut3)
            }
        }
    }
    cutOffset = cuts.position.subtract(icosahedron.position)

    for(let i=0 ; i<5 ; i++) {
        position = new paper.Point(size / 2 + i * size, 0)
        for(let j=0 ; j<2 ; j++) {

            let instance = new paper.SymbolItem(icosahedronDefinition)
            instance.position = position
            if(i==0 && j==0 && cuts != null) {
                cuts.position = instance.position.add(cutOffset)
            }
            if(i>0 && j==0 && cuts != null) {
                let cutsN = cuts.clone()
                cutsN.position.x += i * size
            }
            pieces.addChild(instance)

            position = position.add(size/2, sqrt3o2*size)
        }
    }
    // pieces.addChild(icosahedron)
    pieces.position = paper.view.bounds.center

    let connectors = new paper.Group()
    
    position = paper.view.bounds.center.clone()
    let connector = generateConnector(position, parameters.notchSize, parameters.coreSize, parameters.woodThickness, parameters.connectorHeight, style)
    let connectorDefinition = new paper.SymbolDefinition(connector)

    for(let i=0 ; i<15 ; i++) {
        let width = 0
        for(let j=0 ; j<4 ; j++) {
            let instance = new paper.SymbolItem(connectorDefinition)
            instance.position = position
            connectors.addChild(instance)
            position.y += connector.bounds.height + margin
        }
        position.x += connector.bounds.width + margin
        position.y -= 4 * (connector.bounds.height + margin)
    }

    connectors.position.x = pieces.position.x
    connectors.bounds.top = pieces.bounds.bottom + margin

    pentagonGroup.position = connectors.bounds.bottomCenter.add(0, pentagonGroup.bounds.height / 2 + margin)
}

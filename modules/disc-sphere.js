import { createGuiFromDescription } from './gui.js'

let parameters = {}
let controllers = {}

let parametersDescription = {
    radius: {
        value: 100,
        min: 1,
        max: 300,
    },
    thickness: {
        value: 20,
        min: 0,
        max: 100,
        step: 1
    },
    woodThickness: {
        value: 3,
        min: 0,
        max: 15,
        step: 1
    },
    margin: {
        value: 10,
        min: 0,
        max: 100,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

export function generate() {

    paper.project.activeLayer.removeChildren()

    let sphere = new paper.Group()

    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    // let nCircles = 5
    // let radius = size / 2
    // let thickness = parameters.thickness || 5
    // let inPath = new Path.Arc(new paper.Point(0, 0), new paper.Point(radius, radius), new paper.Point(2*radius, 0));
    // let outPath = new Path.Arc(new paper.Point(-thickness, 0), new paper.Point(radius, radius+thickness), new paper.Point(2*radius+thickness, 0));
    // inPath.insert(0, outPath.firstSegment)
    // inPath.add(outPath.lastSegment)
    
    let radius = parameters.radius
    let thickness = parameters.thickness
    let woodThickness = parameters.woodThickness
    let margin = parameters.margin

    let position = paper.view.bounds.topLeft.add(radius+2*thickness+margin);

    let inCircle = new paper.Path.Circle(position, radius+thickness);
    let outCircle = new paper.Path.Circle(position, radius+2*thickness);

    let createPuzzle = (p, top, thickness)=> {
        top = top ? 1 : -1;
        let path = new paper.Path();
        path.add(p);
        path.add(p.add(thickness/3, 0));
        path.add(p.add(thickness/4, top*thickness/3));
        path.add(p.add(3*thickness/4, top*thickness/3));
        path.add(p.add(2*thickness/3, 0));
        path.add(p.add(thickness, 0));
        return path;
    }
    createPuzzle(outCircle.firstSegment.point, true, thickness);
    createPuzzle(inCircle.lastSegment.previous.point, false, thickness);

    let createVNotch = (p, top)=> {
        top = top ? 1 : -1;
        let path = new paper.Path();

        path.add(p.add(-woodThickness/2, 0));
        path.add(p.add(-woodThickness/2, top*thickness/2));
        path.add(p.add(woodThickness/2, top*thickness/2));
        path.add(p.add(woodThickness/2, 0));
        return path;
    }

    let createVNotch2 = (p, top, circle, right)=> {
        top = top ? 1 : -1;
        right = right ? 1 : -1;
        let path = new paper.Path();

        path.add(p);
        path.add(p.add(-woodThickness*right, 0));
        path.add(p.add(-woodThickness*right, top*thickness/2));
        path.add(p.add(woodThickness*right, top*thickness/2));
        path.add(p.add(woodThickness*right, 0));
        let intersections = path.getIntersections(circle);
        if(intersections.length > 1) {
            path.splitAt(intersections[1]).remove();
        }
        return path;
    }

    let createHNotch = (p, left)=> {
        left = left ? 1 : -1;
        let path = new paper.Path();

        path.add(p.add(0, -woodThickness/2));
        path.add(p.add(left*thickness/2, -woodThickness/2));
        path.add(p.add(left*thickness/2, woodThickness/2));
        path.add(p.add(0, woodThickness/2));
        return path;
    }

    createVNotch(inCircle.firstSegment.next.point, false);
    createVNotch(inCircle.lastSegment.point, true);


    position.x += 2 * radius + 4 * thickness;


    let inCircle2 = new paper.Path.Circle(position, radius);
    let outCircle2 = new paper.Path.Circle(position, radius+2*thickness);


    createPuzzle(outCircle2.firstSegment.point, true, 2*thickness);
    createPuzzle(inCircle2.lastSegment.previous.point, false, 2*thickness);

    createVNotch(outCircle2.firstSegment.next.point, true);
    createVNotch(outCircle2.lastSegment.point, false);

    position.x = paper.view.bounds.left + margin + radius + 2*thickness;
    position.y += 2 * radius + 4 * thickness;


    let nCircles = 6;
    let angle = 0;
    let angleStep = (Math.PI / 2) / nCircles;
    for(let i=0 ; i<nCircles ; i++) {
        let r = radius*Math.cos(angle);
        let group = new paper.Group();


        let inCircle3 = new paper.Path.Circle(position, r);
        let outCircle3 = new paper.Path.Circle(position, r+thickness);
        
        group.addChild(inCircle3);
        group.addChild(outCircle3);
        group.addChild(createVNotch(outCircle3.firstSegment.next.point, true));
        group.addChild(createVNotch(outCircle3.lastSegment.point, false));
        group.addChild(createHNotch(outCircle3.firstSegment.point, true));
        group.addChild(createHNotch(outCircle3.lastSegment.previous.point, false));
        
        let p = inCircle2.position;
        let x = radius*Math.cos(angle+Math.PI/2);
        let y = radius*Math.sin(angle+Math.PI/2);
        
        createVNotch2(p.add(x, y), true, inCircle2, true);
        createVNotch2(p.add(x, -y), false, inCircle2, true);
        
        createVNotch2(p.add(-x, y), true, inCircle2, false);
        createVNotch2(p.add(-x, -y), false, inCircle2, false);
        
        let clone = group.clone();
        clone.position.y += 2 * radius + 2 * thickness;
        
        position.x += 2 * r  + 2 * thickness;
        if(i%2==1) {
            // position.x = view.bounds.left + margin + radius + 2*thickness;
        }
        
        angle += angleStep;
    }


    for(let child of paper.project.activeLayer.children) {
        child.style = style;
        if(child.children) {
            for(let greatchild of child.children) {
                greatchild.style = style;
            }
        }
    }

}
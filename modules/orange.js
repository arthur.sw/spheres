import { createGuiFromDescription } from './gui.js'
import { drawTriangle, getLastTriangle } from './sphere.js'

let parameters = {}
let controllers = {}

let material = new THREE.MeshPhongMaterial( { color: new THREE.Color(Math.random(), Math.random(), Math.random()), emissive: 0x072534, side: THREE.DoubleSide, flatShading: true, transparent: true, opacity: 0.5 } )

let scene = null
let group = null

let parametersDescription = {
    size: {
        value: 100,
        min: 1,
        max: 300,
    },
    nIterations: {
        value: 100,
        min: 10,
        max: 400,
        step: 1
    },
    nTurns: {
        value: 5,
        min: 1,
        max: 10,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}


export let drawLastTriangle = (indices, vertices, sphere, p0, p2, firstTriangle, angleOffset)=> {
    let triangle = getLastTriangle(indices)
    return drawTriangle(triangle, vertices, sphere, p0, p2, firstTriangle, undefined, angleOffset)
}

export let createLineFromPath = (path, sphere)=> {
    let line = new paper.Path()
    line.strokeColor = path.strokeColor
    line.strokeWidth = path.strokeWidth
    sphere.addChild(line)
    return line
}


let drawLines = (path1, path2, sphere, inside)=> {

    let step = 0.3
    for(let i=0 ; i<path1.getLength()/1.65 ; i+=step) {
        
        let line1 = createLineFromPath(path1, sphere)

        let normal1 = path1.getNormalAt(inside ? i : path1.getLength() - i)
        let point1 = path1.getPointAt(inside ? i : path1.getLength() - i)
        if(normal1 != null) {
            line1.add(point1)
            line1.add(point1.add(normal1.multiply(!inside ? 0.5 : -0.5)))
        }

        let line2 = createLineFromPath(path2, sphere)

        let normal2 = path1.getNormalAt(inside ? i+step/2 : path1.getLength() - i - step/2)
        let point2 = path1.getPointAt(inside ? i+step/2 : path1.getLength() - i - step/2)
        if(normal2 != null) {
            let closeToTargetPoint = point2.add(normal2.multiply(!inside ? 0.5 : -0.5))

            line2.add(point2.add(normal2.multiply(!inside ? 0.1 : -0.1)))
            line2.add(path2.getNearestPoint(closeToTargetPoint))

        }

    }
}

let drawTopBottomPath = (path, sphere, top)=> {
    
    let nSegmentsToIgnore = 10
    let lastIndex = path.segments.length - 1

    let maxIndex = top ? lastIndex : lastIndex - nSegmentsToIgnore
    let minIndex = top ? nSegmentsToIgnore : 0

    for(let segment of path.segments) {

        if(segment.index <= minIndex || segment.index >= maxIndex) {
            continue
        }

        let line = createLineFromPath(path, sphere)
        
        let endSegment = top ? path.firstSegment : path.lastSegment

        // if(top && segment.index == maxIndex - 1 || !top && segment.index == minIndex + 1) {
            line.add(endSegment.point)
        // }

        let delta = segment.point.subtract(endSegment.point)
        let length = delta.length
        delta = delta.normalize()
        // let i = top ? 1 - segment.index / path.segments.length : segment.index / path.segments.length
        // line.add(endSegment.point.add(delta.multiply(length*0.5*i)))
        // line.add(endSegment.point.add(delta.multiply(length*0.2)))
        line.add(endSegment.point.add(delta.multiply(length*0.7)))
    }
}

export function generate(v=null, s=scene, g=group) {

    scene = s
    group = g

    for(let c of group.children) {
        group.remove(c)
    }

    paper.project.activeLayer.removeChildren()

    let sphere = new paper.Group()

    let size = parameters.size

    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    let vertices = []
    let normals = []
    let colors = []

    let indices = []


    let theta = 0
    let phi = Math.PI / 2

    let nIterations = parameters.nIterations
    let nTurns = parameters.nTurns

    let nStepsPerTurns = Math.floor(nIterations / nTurns)

    let thetaStep = nTurns * 2 * Math.PI / nIterations
    let phiStep = Math.PI / nIterations
    let radius = 1

    let center = new THREE.Vector3(0, 0, 0)

    let p0 = null
    let p2 = null

    let path1 = new paper.Path()
    let path2 = new paper.Path()
    path1.strokeColor = 'black'
    path1.strokeWidth = 1
    path2.strokeColor = 'black'
    path2.strokeWidth = 1
    sphere.addChild(path1)
    sphere.addChild(path2)

    let topPath = new paper.Path()
    topPath.strokeColor = 'black'
    topPath.strokeWidth = 1
    sphere.addChild(topPath)

    let bottomPath = new paper.Path()
    bottomPath.strokeColor = 'black'
    bottomPath.strokeWidth = 1
    sphere.addChild(bottomPath)


    for(let i=0 ; i<nIterations ; i++) {
        let r = radius * Math.cos(phi)
        let x = r * Math.cos(theta)
        let y = r * Math.sin(theta)
        let h = radius * Math.sin(phi)
        
        vertices.push(new THREE.Vector3(x, h, y))
        
        // console.log('step ' + i + ': ')

        let previousIndex = Math.max(i - nStepsPerTurns, 0)
        if(i <= nStepsPerTurns) {
            if(i>=2) {

                indices.push(0)
                indices.push(i)
                indices.push(i - 1)

                let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, i==2 )
                p0 = np0
                p2 = np1

            }
            if(i == nStepsPerTurns) {

                indices.push(0)
                indices.push(1)
                indices.push(i)

                let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, i==2 )
                p0 = np1
                p2 = np2
            }
        } else {

            indices.push(i - nStepsPerTurns - 1)
            indices.push(i)
            indices.push(i - 1)
            
            let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
            p0 = np0
            p2 = np1

            // console.log(i, i - nStepsPerTurns - 1, i-1)

            indices.push(i - nStepsPerTurns - 1)
            indices.push(i - nStepsPerTurns)
            indices.push(i)
            
            let [n2p0, n2p1, n2p2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
            p0 = n2p1
            p2 = n2p2

            // console.log(i, i - nStepsPerTurns, i - nStepsPerTurns - 1)

        }

        if(i>nStepsPerTurns && p0 != null && !p0.isNaN()) {
            path1.add(p0)
        }

        if(i>=2 && i<=nStepsPerTurns+1 && p2 != null && !p2.isNaN()) {
            topPath.add(p2)
        }

        if(i>nStepsPerTurns && p2 != null && !p2.isNaN()) {
            path2.add(p2)
        }

        // console.log('-----')

        theta += thetaStep
        phi -= phiStep

    }

    if(p0 != null && !p0.isNaN()) {
        bottomPath.add(p0)
    }

    for(let i=nIterations ; i<nIterations+nStepsPerTurns && nTurns > 1 ; i++) {
        
        indices.push(i - nStepsPerTurns - 1)
        indices.push(i - nStepsPerTurns)
        indices.push(nIterations - 1)

        let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
        p0 = np1
        p2 = np2

        if(p0 != null && !p0.isNaN()) {
            bottomPath.add(p0)
        }
    }

    path2.smooth()
    path1.smooth()
    drawLines(path2, path1, sphere, true)
    drawLines(path1, path2, sphere, false)

    drawTopBottomPath(topPath, sphere, true)
    drawTopBottomPath(bottomPath, sphere, false)


    let geometry = new THREE.BufferGeometry()
    geometry.setIndex( indices )
    let vs = []
    let ns = []
    let cs = []
    for(let i=0 ; i<vertices.length ; i++) {
        vs.push(vertices[i].x*parameters.size, vertices[i].y*parameters.size, vertices[i].z*parameters.size)
        let normal = vertices[i].clone().sub(center).normalize()
        ns.push(normal.x, normal.y, normal.z)
        cs.push(0, 1, 0)
    }
    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vs, 3 ) )
    geometry.addAttribute( 'normal', new THREE.Float32BufferAttribute( ns, 3 ) )
    geometry.addAttribute( 'color', new THREE.Float32BufferAttribute( cs, 3 ) )

    let mesh = new THREE.Mesh( geometry, material )

    group.add( mesh )
    scene.add( group )

    sphere.position = paper.view.bounds.center
    let margin = 20
    let sphereRatio = sphere.bounds.width / sphere.bounds.height
    let viewRatio = paper.view.bounds.width / paper.view.bounds.height
    if(sphereRatio > viewRatio) {
        sphere.scale( ( paper.view.bounds.width - margin ) / sphere.bounds.width)
    } else {
        sphere.scale( ( paper.view.bounds.height - margin ) / sphere.bounds.height)
    }

    for(let child of sphere.children.slice()) {
        if(child.data != null && child.data.name == 'triangle') {
            child.remove()
        }
    }
}

import { createGuiFromDescription } from './gui.js'

let parameters = {}
let controllers = {}

let parametersDescription = {
    size: {
        value: 100,
        min: 1,
        max: 100,
    },
    stepLength: {
        value: 16,
        min: 1,
        max: 50,
    },
    stepHeight: {
        value: 8,
        min: 1,
        max: 50,
    }
}

export function generateLine(a, b, style, icosahedron, p) {
    if(p == null) {
        p = new paper.Path()
        p.add(a)
    }
    p.add(b)
    p.style = style
    icosahedron.addChild(p)
    return p
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

let sqrt3o2 = Math.sqrt(3) / 2

export function generate() {
    paper.project.activeLayer.removeChildren()

    let icosahedron = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }
    for(let i=0 ; i<5 ; i++) {
        let position = new paper.Point(size / 2 + i * size, 0)
        for(let j=0 ; j<4 ; j++) {
            if(j == 2) {
                position = position.add(size/2, size)
            }

            let p0 = j%2 == 0 ? position.clone() : position.add(0, 2 * size * sqrt3o2)
            let p1 = position.add(size / 2, size * sqrt3o2)
            let p2 = position.add(-size / 2, size * sqrt3o2)
            
            let center = p0.add(p1).add(p2).divide(3)

            let delta0 = p1.subtract(p0).normalize()
            let delta1 = p2.subtract(p1).normalize()
            let delta2 = p0.subtract(p2).normalize()

            let length = size

            if(j == 0 || j == 3) {
                generateLine(p0, p1, style, icosahedron)
            } else {
                generateLine(p0, p0.add(delta0.multiply(length / 2 - parameters.stepLength / 2)), style, icosahedron)
                generateLine(p0.add(delta0.multiply(length / 2 + parameters.stepLength / 2)), p1, style, icosahedron)
            }
            
            if(i==0 && j == 1) {
                generateLine(p0, p2, style, icosahedron)
            } else if(i==4 && j == 2) {
                generateLine(p0, p1, style, icosahedron)
            } else {
                generateLine(p1, p1.add(delta1.multiply(length / 2 - parameters.stepLength / 2)), style, icosahedron)
                generateLine(p1.add(delta1.multiply(length / 2 + parameters.stepLength / 2)), p2, style, icosahedron)
            }

            if(j == 0 || j == 3) {
                generateLine(p2, p0, style, icosahedron)
            } else {
                generateLine(p2, p2.add(delta2.multiply(length / 2 - parameters.stepLength / 2)), style, icosahedron)
                generateLine(p2.add(delta2.multiply(length / 2 + parameters.stepLength / 2)), p0, style, icosahedron)
            }

            let p0Center = center.subtract(p0).normalize()
            let p1Center = center.subtract(p1).normalize()
            let p2Center = center.subtract(p2).normalize()

            let p01 = p0.add(p0Center.multiply(parameters.stepHeight))
            let p11 = p1.add(p1Center.multiply(parameters.stepHeight))
            let p21 = p2.add(p2Center.multiply(parameters.stepHeight))

            let delta01 = delta0.multiply(parameters.stepLength / 2)
            let delta11 = delta1.multiply(parameters.stepLength / 2)
            let delta12 = delta2.multiply(parameters.stepLength / 2)

            generateLine(p01.add(delta01), p11.subtract(delta01), style, icosahedron)
            generateLine(p11.add(delta11), p21.subtract(delta11), style, icosahedron)
            generateLine(p21.add(delta12), p01.subtract(delta12), style, icosahedron)

            let p02 = p0.add(p0Center.multiply(2 * parameters.stepHeight))
            let p12 = p1.add(p1Center.multiply(2 * parameters.stepHeight))
            let p22 = p2.add(p2Center.multiply(2 * parameters.stepHeight))
            
            let length2 = p12.subtract(p02).getLength()

            generateLine(p02, p02.add(delta0.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)
            generateLine(p12, p12.subtract(delta0.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)
            
            generateLine(p12, p12.add(delta1.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)
            generateLine(p22, p22.subtract(delta1.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)

            generateLine(p22, p22.add(delta2.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)
            generateLine(p02, p02.subtract(delta2.multiply(length2 / 2 - parameters.stepLength / 2)), style, icosahedron)

        }
    }

    icosahedron.position = paper.view.bounds.center //.subtract(icosahedron.bounds.size.divide(2))
}

import { createGuiFromDescription } from './gui.js'
import { getLastTriangle } from './sphere.js'
import { drawLastTriangle, createLineFromPath } from './orange.js'

let parameters = {}
let controllers = {}

let material = new THREE.MeshPhongMaterial( { color: new THREE.Color(Math.random(), Math.random(), Math.random()), emissive: 0x072534, side: THREE.DoubleSide, flatShading: true, transparent: true, opacity: 0.5 } )

let scene = null
let group = null

let parametersDescription = {
    size: {
        value: 100,
        min: 1,
        max: 300,
    },
    nSlices: {
        value: 20,
        min: 3,
        max: 50,
        step: 1
    },
    nPetals: {
        value: 8,
        min: 2,
        max: 20,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}


export function generate(v=null, s=scene, g=group) {

    scene = s
    group = g

    for(let c of group.children) {
        group.remove(c)
    }

    paper.project.activeLayer.removeChildren()

    let sphere = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    let vertices = []
    let normals = []
    let colors = []

    let indices = []

    let nPetals = parameters.nPetals
    let nSlices = parameters.nSlices

    let thetaStep = 2 * Math.PI / nPetals
    let phiStep = Math.PI / nSlices
    let radius = 1

    let center = new THREE.Vector3(0, 0, 0)

    let p0 = null
    let p2 = null

    let theta = 0
    let phi = Math.PI / 2

    let indicesMap = []
    let paths = []

    let circleRadius = 0.33

    let woodFlexibleThickness = 0.1

    for(let i = 0 ; i <= nPetals ; i++) {
        
        indicesMap.push([])

        phi = Math.PI / 2

        let path1 = null
        let path2 = null

        if(i > 0) {
            path1 = new paper.Path()
            path1.strokeColor = 'black'
            path1.strokeWidth = 1
            sphere.addChild(path1)
            
            path1.add(0, 0)

            path2 = new paper.Path()
            path2.strokeColor = 'black'
            path2.strokeWidth = 1
            sphere.addChild(path2)

            path2.add(0, 0)

            paths.push(path1)
            paths.push(path2)
        }

        for(let j = 0 ; j <= nSlices ; j++) {

            let r = radius * Math.cos(phi)
            let x = r * Math.cos(theta)
            let y = r * Math.sin(theta)
            let h = radius * Math.sin(phi)

            if((i == 0 || j > 0 && j < nSlices ) && i < nPetals) {
                vertices.push(new THREE.Vector3(x, h, y))
            }

            let index = j==0 ? 0 : j == nSlices ? nSlices : vertices.length-1

            indicesMap[i].push(i < nPetals ? index : j)

            console.log(i, j, indicesMap[i][j])

            if(i > 0) {

                if(j == 1) {

                    indices.push(0)
                    indices.push(indicesMap[i][j])
                    indices.push(indicesMap[i-1][j])

                    let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, true, theta )
                    p0 = np1
                    p2 = np2

                    console.log(getLastTriangle(indices))

                    path1.add(p0)
                    path2.add(p2)

                } else if(j > 1 && j < nSlices) {

                    indices.push(indicesMap[i][j-1])
                    indices.push(indicesMap[i][j])
                    indices.push(indicesMap[i-1][j-1])

                    let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
                    p0 = np1
                    p2 = np2
                    console.log(getLastTriangle(indices))

                    indices.push(indicesMap[i][j])
                    indices.push(indicesMap[i-1][j])
                    indices.push(indicesMap[i-1][j-1])

                    let [n2p0, n2p1, n2p2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
                    p0 = n2p0
                    p2 = n2p1
                    console.log(getLastTriangle(indices))

                    path1.add(np1)
                    path2.add(n2p1)

                    let point1 = np1
                    let point2 = n2p1

                    let delta = point2.subtract(point1)
                    let length = delta.length
                    delta = delta.normalize()

                    if(j % 2 == 0) {
                        let line1 = createLineFromPath(path1, sphere)
                        line1.add(point1)
                        line1.add(point1.add(delta.multiply(length / 2 - woodFlexibleThickness)))
                        let line2 = createLineFromPath(path2, sphere)
                        line2.add(point2)
                        line2.add(point2.add(delta.multiply(-length / 2 + woodFlexibleThickness)))
                    } else {
                        let line = createLineFromPath(path1, sphere)
                        line.add(point1.add(delta.multiply(woodFlexibleThickness)))
                        line.add(point2.add(delta.multiply(-woodFlexibleThickness)))
                    }
                    

                } else if(j == nSlices) {

                    indices.push(indicesMap[i][j-1])
                    indices.push(indicesMap[i][j])
                    indices.push(indicesMap[i-1][j-1])

                    let [np0, np1, np2] = drawLastTriangle(indices, vertices, sphere, p0, p2, false)
                    p0 = np1
                    p2 = np2
                    console.log(getLastTriangle(indices))

                    path1.add(np1)
                    path2.add(np1)
                }

                
            }


            phi -= phiStep
        }

        theta += thetaStep
    }

    let circle = new paper.Path.Circle(new paper.Point(0, 0), circleRadius)
    circle.strokeWidth = 1
    circle.strokeColor = 'grey'
    sphere.addChild(circle)

    for(let p of paths) {
        let intersections = p.getIntersections(circle)
        
        for(let intersection of intersections) {
            let remaining = p.splitAt(intersection)
            p.remove()
        }
    }

    // path2.smooth()
    // path1.smooth()
    // drawLines(path2, path1, sphere, true)
    // drawLines(path1, path2, sphere, false)

    // drawTopBottomPath(topPath, sphere, true)
    // drawTopBottomPath(bottomPath, sphere, false)


    let geometry = new THREE.BufferGeometry()
    geometry.setIndex( indices )
    let vs = []
    let ns = []
    let cs = []
    for(let i=0 ; i<vertices.length ; i++) {
        vs.push(vertices[i].x*parameters.size, vertices[i].y*parameters.size, vertices[i].z*parameters.size)
        let normal = vertices[i].clone().sub(center).normalize()
        ns.push(normal.x, normal.y, normal.z)
        cs.push(0, 1, 0)
    }
    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vs, 3 ) )
    geometry.addAttribute( 'normal', new THREE.Float32BufferAttribute( ns, 3 ) )
    geometry.addAttribute( 'color', new THREE.Float32BufferAttribute( cs, 3 ) )

    let mesh = new THREE.Mesh( geometry, material )

    group.add( mesh )
    scene.add( group )

    sphere.position = paper.view.bounds.center
    let margin = 20
    let sphereRatio = sphere.bounds.width / sphere.bounds.height
    let viewRatio = paper.view.bounds.width / paper.view.bounds.height
    if(sphereRatio > viewRatio) {
        sphere.scale( ( paper.view.bounds.width - margin ) / sphere.bounds.width)
    } else {
        sphere.scale( ( paper.view.bounds.height - margin ) / sphere.bounds.height)
    }
    
    for(let child of sphere.children.slice()) {
        if(child.data != null && child.data.name == 'triangle') {
            child.remove()
        }
    }
}

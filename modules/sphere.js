import { createGuiFromDescription } from './gui.js'
import { generateLine } from './icosahedron.js'

let parameters = {}
let controllers = {}

let triangleMap = null

let material = new THREE.MeshPhongMaterial( { color: new THREE.Color(Math.random(), Math.random(), Math.random()), emissive: 0x072534, side: THREE.DoubleSide, flatShading: true, transparent: true, opacity: 0.5 } )

let scene = null
let group = null

let parametersDescription = {
    size: {
        value: 100,
        min: 1,
        max: 300,
    },
    nIterations: {
        value: 3,
        min: 0,
        max: 4,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

function getVertex(i0, i1, indexMap) {

    let i0m = indexMap.get(i0)
    if(i0m != null) {
        return i0m.get(i1)
    }
    return null
}

function setVertex(i0, i1, indexMap, v, i) {

    let object = {i: i, v: v}

    let i0m = indexMap.get(i0)
    if(i0m == null) {
        i0m = new Map()
        indexMap.set(i0, i0m)
    }
    i0m.set(i1, object)

    let i1m = indexMap.get(i1)
    if(i1m == null) {
        i1m = new Map()
        indexMap.set(i1, i1m)
    }
    i1m.set(i0, object)

    return object
}

function setVertexInIndexMap(i0, i1, indices, indexMap, vertices, center, size) {
    let v0 = vertices[i0]
    let v1 = vertices[i1]

    let object = getVertex(i0, i1, indexMap)

    if(object == null) {
        let v01 = v0.clone().add(v1).multiplyScalar(0.5)
        let centerToV01 = v01.clone().sub(center).normalize().multiplyScalar(size)
        let v = center.clone().add(centerToV01)
        vertices.push(v)
        let i = vertices.length - 1
        object = setVertex(i0, i1, indexMap, v, i)
    }

    return object
}

function setVertexInTriangleMap(i0, i1, i2, triangleMap) {
    let i0m = triangleMap.get(i0)
    if(i0m == null) {
        i0m = new Map()
        triangleMap.set(i0, i0m)
    }
    let i1m = i0m.get(i1)
    if(i1m == null) {
        i0m.set(i1, [i2])
    } else {
        i1m.push(i2)
    }
}

function setSegmentInTriangleMap(i0, i1, i2, triangleMap) {
    setVertexInTriangleMap(i0, i1, i2, triangleMap)
    setVertexInTriangleMap(i1, i0, i2, triangleMap)
}

function setIndicesInTriangleMap(i0, i1, i2, e0, e1, e2, triangleMap) {
    if(e0) {
        setSegmentInTriangleMap(i0, i1, i2, triangleMap)
    }
    if(e1) {
        setSegmentInTriangleMap(i1, i2, i0, triangleMap)
    }
    if(e2) {
        setSegmentInTriangleMap(i2, i0, i1, triangleMap)
    }
}

function subdivideAndSpherify(i0, i1, i2, e0, e1, e2, indices, indexMap, vertices, center, size, n) {
    
    if(n == 0) {
        indices.push(i0)
        indices.push(i1)
        indices.push(i2)
        // console.log(i0, i1, i2, e0, e1, e2)
        setIndicesInTriangleMap(i0, i1, i2, e0, e1, e2, triangleMap)
        return 
    }

    let o01 = setVertexInIndexMap(i0, i1, indices, indexMap, vertices, center, size)
    let o12 = setVertexInIndexMap(i1, i2, indices, indexMap, vertices, center, size)
    let o20 = setVertexInIndexMap(i2, i0, indices, indexMap, vertices, center, size)
    // console.log("-----")
    // console.log(i0, i1, i2)
    // console.log(vertices[i0], vertices[i1], vertices[i2])
    // console.log(o01.v, o01.i)
    // console.log(o12.v, o12.i)
    // console.log(o20.v, o20.i)
    // console.log("-----")

    subdivideAndSpherify(i0, o01.i, o20.i, false, true, e2, indices, indexMap, vertices, center, size, n-1)
    subdivideAndSpherify(o01.i, i1, o12.i, e0, false, true, indices, indexMap, vertices, center, size, n-1)
    subdivideAndSpherify(o20.i, o12.i, i2, true, e1, false, indices, indexMap, vertices, center, size, n-1)
    subdivideAndSpherify(o12.i, o01.i, o20.i, true, true, true, indices, indexMap, vertices, center, size, n-1)
}

let getTriangleIndicesFromEdge = (i0, i1)=> {
    let i0m = triangleMap.get(i0)
    if(i0m == null) {
        return null
    }
    return i0m.get(i1)
}

let getNeighborTriangle = (i0, i1, i2)=> {
    let indices = getTriangleIndicesFromEdge(i0, i1)
    if(indices == null) {
        return null
    }
    let im = indices.find((i)=> i != i2)
    return im != null ? [i0, im, i1] : null
}

let getNeighborTriangleFromEdge = (triangle, edge)=> {
    if(edge == 0) {
        return getNeighborTriangle(triangle[0], triangle[1], triangle[2])
    }
    else if(edge == 1) {
        return getNeighborTriangle(triangle[1], triangle[2], triangle[0])
    }
    else {
        return getNeighborTriangle(triangle[2], triangle[0], triangle[1])
    }
}

let drawIndex = (p, i)=> {
    var text = new paper.PointText(p)
    text.justification = 'center'
    text.fontSize = 0.05
    text.fillColor = 'green'
    text.content = ''+i
    sphere.addChild(text)
}

let edgeHasTwoTriangles = (i0, i1)=> {
    let indices = getTriangleIndicesFromEdge(i0, i1)
    if(indices == null) {
        return null
    }
    return indices.length >= 2
}

let drawThickEdge = (p0, p1, sphere)=> {
    let p = new paper.Path()
    p.strokeWidth = 1
    p.strokeColor = 'black'
    p.add(p0)
    p.add(p1)
    sphere.addChild(p)
}

export let drawTriangle = (triangle, vertices, sphere, p0, p2, firstTriangle=false, path = new paper.Path(), angleOffset = 0, drawCircle = false)=> {
    
    path.strokeWidth = .5
    path.strokeColor = 'red'
    path.closed = true
    path.data.name = 'triangle'

    let v0 = vertices[triangle[0]]
    let v1 = vertices[triangle[1]]
    let v2 = vertices[triangle[2]]
    let v01 = v1.clone().sub(v0)
    let v02 = v2.clone().sub(v0)
    let v01n = v01.clone().normalize()
    let v02n = v02.clone().normalize()
    let theta = Math.acos(v01n.dot(v02n))
    
    // let a1 = v02n.dot(v01)
    // let h = v01.clone().sub(v02n.clone().multiplyScalar(a1)).length()
    
    if(firstTriangle) {
        let a = v02.length()
        p0 = new paper.Point(0, 0)
        p2 = new paper.Point(0, a)
        p2.angleInRadians += angleOffset
    }

    let p02 = p2.subtract(p0)
    let p02n = p02.normalize()
    p02n.angleInRadians += theta

    let p1 = p0.add(p02n.multiply(v01.length()))
    path.add(p0)
    path.add(p1)
    path.add(p2)
    
    sphere.addChild(path)
    
    if(drawCircle) {
        let circle = new paper.Path.Circle(p0.add(p1).add(p2).divide(3), p1.subtract(p0).length / 8)

        circle.strokeWidth = 1
        circle.strokeColor = 'black'
        sphere.addChild(circle)
    }

    return [p0, p1, p2]
}

let drawTriangles = (triangle, vertices, sphere, p0, p2, firstTriangle=false)=> {

    let path = new paper.Path()
    let [np0, np1, np2] = drawTriangle(triangle, vertices, sphere, p0, p2, firstTriangle, path, 0, true)

    let p1 = np1

    if(!edgeHasTwoTriangles(triangle[0], triangle[1])) {
        drawThickEdge(p0, p1, sphere)
    }
    if(!edgeHasTwoTriangles(triangle[1], triangle[2])) {
        drawThickEdge(p1, p2, sphere)
    }
    if(!edgeHasTwoTriangles(triangle[2], triangle[0])) {
        drawThickEdge(p2, p0, sphere)
    }
   

    // drawIndex(p0, triangle[0])
    // drawIndex(p1, triangle[1])
    // drawIndex(p2, triangle[2])

    let nextTriangle = getNeighborTriangleFromEdge(triangle, 0)
    if(nextTriangle) {
        p0 = path.segments[0].point
        p2 = path.segments[1].point
        drawTriangles(nextTriangle, vertices, sphere, p0, p2)
    }
    nextTriangle = getNeighborTriangleFromEdge(triangle, 1)
    if(nextTriangle) {
        p0 = path.segments[1].point
        p2 = path.segments[2].point
        drawTriangles(nextTriangle, vertices, sphere, p0, p2)
    }
    if(firstTriangle) {
        nextTriangle = getNeighborTriangleFromEdge(triangle, 2)
        if(nextTriangle) {
            p0 = path.segments[2].point
            p2 = path.segments[0].point
            drawTriangles(nextTriangle, vertices, sphere, p0, p2)
        }
    }
}

export let getLastTriangle = (indices)=> {
    return [indices[indices.length-3], indices[indices.length-2], indices[indices.length-1]]
}

export function generate(v=null, s=scene, g=group) {
    scene = s
    group = g

    for(let c of group.children) {
        group.remove(c)
    }
    
    triangleMap = new Map()

    paper.project.activeLayer.removeChildren()

    let sphere = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }

    let vertices = []
    let normals = []
    let colors = []
    let indexMap = new Map()


    let hy = 1 / (2 * Math.sin(Math.PI / 5))
    let theta = Math.asin(hy)
    let hy2 = Math.sqrt(1 - hy * hy)
    // let a = hy + 0.5 / Math.cos(theta) - hy2
    let a = 0.5 / Math.cos(theta)

    // let ad = 1 / (2 * Math.cos(3 * Math.PI / 10))
    // let h = Math.sqrt(1 - ad * ad)
    // let h = Math.cos(Math.PI / 3)

    // normals.push(new THREE.Vector3(0, -1, 0))
    // colors.push(new THREE.Vector3(0, 1, 0))
    let center = new THREE.Vector3(0, 0, 0)

    let position = new THREE.Vector3(0, -a, 0)
    vertices.push(position)

    // console.log("-----")
    for(let i=0 ; i<2 ; i++) {
        for(let j=0 ; j<5 ; j++) {
            let angle = j * 2 * Math.PI / 5 + i * 2 * Math.PI / 10
            vertices.push(new THREE.Vector3(hy * Math.cos(angle), i==0 ? -a + hy2 : a - hy2, hy * Math.sin(angle)))
            // normals.push(vertices[vertices.length-1].clone().sub(center).normalize())
            // colors.push(new THREE.Vector3(0, 1, 0))
            // console.log(vertices[vertices.length-1])
        }
        // console.log("-----")
    }
    vertices.push(new THREE.Vector3(0, a, 0))
    // normals.push(new THREE.Vector3(0, 1, 0))
    // colors.push(new THREE.Vector3(0, 1, 0))

    // console.log(vertices[vertices.length-1])

    // console.log("-----")
    // console.log("-----")
    // for(let v of vertices) {
    //     console.log(v)
    // }
    // console.log(vertices.length)
    // console.log("-----")
    // console.log("-----")

    let indices = []
    let n = parameters.nIterations

    let iTop = vertices.length - 1

    for(let i=0 ; i<4 ; i++) {
        for(let j=0 ; j<5 ; j++) {
            let triangleIndices = []
            let e0 = false
            let e1 = false
            let e2 = false
            if(i==0) {
                triangleIndices.push(0)
                triangleIndices.push(j+1)
                triangleIndices.push((j+1)%5+1)
                e1 = true
            } else if (i==1) {
                triangleIndices.push(j+1)
                triangleIndices.push((j+1)%5+1)
                triangleIndices.push(j+5+1)
                e0 = true
                e1 = j < 4
                e2 = true
            } else if (i==2) {
                triangleIndices.push(j + 1)
                triangleIndices.push(( j == 0 ? 4 : j - 1) + 5 + 1)
                triangleIndices.push(j + 5 + 1)
                e0 = j > 0
                e1 = true
                e2 = true
            } else if (i==3) {
                triangleIndices.push((j+1)%5+5+1)
                triangleIndices.push(iTop)
                triangleIndices.push(j + 1 + 5)
                e2 = true
            }
            subdivideAndSpherify(triangleIndices[0], triangleIndices[1], triangleIndices[2], e0, e1, e2, indices, indexMap, vertices, center, a, n)

            // console.log(triangleIndices, e0, e1, e2)
        }
        // console.log("-----")
    }
    

    let triangle = getLastTriangle(indices)
    
    drawTriangles(triangle, vertices, sphere, null, null, true)

    let geometry = new THREE.BufferGeometry()
    geometry.setIndex( indices )
    let vs = []
    let ns = []
    let cs = []
    for(let i=0 ; i<vertices.length ; i++) {
        vs.push(vertices[i].x*parameters.size, vertices[i].y*parameters.size, vertices[i].z*parameters.size)
        let normal = vertices[i].clone().sub(center).normalize()
        ns.push(normal.x, normal.y, normal.z)
        cs.push(0, 1, 0)
    }
    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vs, 3 ) )
    geometry.addAttribute( 'normal', new THREE.Float32BufferAttribute( ns, 3 ) )
    geometry.addAttribute( 'color', new THREE.Float32BufferAttribute( cs, 3 ) )

    let mesh = new THREE.Mesh( geometry, material )

    group.add( mesh )
    scene.add( group )

    sphere.position = paper.view.bounds.center
    let margin = 20
    let sphereRatio = sphere.bounds.width / sphere.bounds.height
    let viewRatio = paper.view.bounds.width / paper.view.bounds.height
    if(sphereRatio > viewRatio) {
        sphere.scale( ( paper.view.bounds.width - margin ) / sphere.bounds.width)
    } else {
        sphere.scale( ( paper.view.bounds.height - margin ) / sphere.bounds.height)
    }
    
}
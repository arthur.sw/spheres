import { createGuiFromDescription } from './gui.js'
import { generateLine } from './icosahedron.js'

let parameters = {}
let controllers = {}

let parametersDescription = {
    size: {
        value: 200,
        min: 1,
        max: 300,
    },
    nIterations: {
        value: 3,
        min: 0,
        max: 4,
        step: 1
    }
}

export function initializeGUI(gui) {

    createGuiFromDescription(gui, parametersDescription, parameters, controllers, generate)

}

let sqrt3o2 = Math.sqrt(3) / 2

function generateTriangle(p0, p1, p2, n, style, icosahedron) {
    let size = p1.subtract(p0).getLength()
    let delta0 = p1.subtract(p0).normalize()
    let delta1 = p2.subtract(p1).normalize()
    let delta2 = p0.subtract(p2).normalize()

    generateLine(p0, p0.add(delta0.multiply(size/2)), style, icosahedron)
    generateLine(p1, p1.add(delta1.multiply(size/2)), style, icosahedron)
    generateLine(p2, p2.add(delta2.multiply(size/2)), style, icosahedron)
    
    if( n > 0 ) {
        let p01 = p0.add(p1).divide(2)
        let p12 = p1.add(p2).divide(2)
        let p20 = p2.add(p0).divide(2)

        generateTriangle(p0, p01, p20, n-1, style, icosahedron)
        generateTriangle(p01, p1, p12, n-1, style, icosahedron)
        generateTriangle(p12, p2, p20, n-1, style, icosahedron)
        generateTriangle(p12, p01, p20, n-1, style, icosahedron)
    }
}

export function generate() {
    paper.project.activeLayer.removeChildren()

    let icosahedron = new paper.Group()

    let size = parameters.size
    let style = {
        strokeColor: 'black',
        strokeWidth: 1
    }
    for(let i=0 ; i<5 ; i++) {
        let position = new paper.Point(size / 2 + i * size, 0)
        for(let j=0 ; j<4 ; j++) {
            if(j == 2) {
                position = position.add(size/2, size * sqrt3o2)
            }

            let p0 = j%2 == 0 ? position.clone() : position.add(0, 2 * size * sqrt3o2)
            let p1 = position.add(size / 2, size * sqrt3o2)
            let p2 = position.add(-size / 2, size * sqrt3o2)
            
            if(j == 0 || j == 3) {
                generateLine(p0, p1, style, icosahedron)
            }

            if(i==0 && j == 1) {
                generateLine(p0, p2, style, icosahedron)
            } else if(i==4 && j == 2) {
                generateLine(p0, p1, style, icosahedron)
            }

            if(j == 0 || j == 3) {
                generateLine(p2, p0, style, icosahedron)
            }

            generateTriangle(p0, p1, p2, Math.floor(parameters.nIterations), style, icosahedron)
        }
    }

    icosahedron.position = paper.view.bounds.center
}
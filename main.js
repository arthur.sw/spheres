import * as icosahedron from "./modules/icosahedron.js"
import * as puzzle from "./modules/puzzle.js"
import * as fractal_icosahedron from "./modules/fractal-icosahedron.js"
import * as cube from "./modules/cube.js"
import * as sphere from "./modules/sphere.js"
import * as orange from "./modules/orange.js"
import * as flower from "./modules/flower.js"
import * as disc_sphere from "./modules/disc-sphere.js"
import * as glasses from "./modules/glasses.js"

let generators = new Map()
generators.set('icosahedron', icosahedron)
generators.set('puzzle', puzzle)
generators.set('fractal-icosahedron', fractal_icosahedron)
generators.set('cube', cube)
generators.set('sphere', sphere)
generators.set('orange', orange)
generators.set('flower', flower)
generators.set('disc-sphere', disc_sphere)
generators.set('glasses', glasses)

let parameters = {
    generator: 'puzzle',
    exportSVG: ()=> {
        
        let svg = paper.project.exportSVG( { asString: true });

        // create an svg image, create a link to download the image, and click it
        let blob = new Blob([svg], {type: 'image/svg+xml'});
        let url = URL.createObjectURL(blob);
        let link = document.createElement("a");
        document.body.appendChild(link);
        link.download = 'result.svg';
        link.href = url;
        link.click();
        document.body.removeChild(link);

    }
}


let camera = null
let controls = null
let container = document.body
let scene, renderer
let materials = []
let meshesInOrder = []
let group = null
let bspDelta = 0.1 
let nPieces = 0

let paperGroup = null
let position = null
let lineHeight = 0

function initialize() {

    group = new THREE.Group()

    paperGroup = new paper.Group()
    position = new paper.Point(0, 0)
    paperGroup.strokeWidth = 1
    paperGroup.strokeColor = 'black'

    container = document.createElement( 'div' )
    document.body.insertBefore( container, document.body.firstChild )

    scene = new THREE.Scene()
    // scene.background = new THREE.Color( 0x000000 )
    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 25, 1000 )
    camera.position.set( 400, 200, 0 );
    
    renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true, alpha: true } )

    renderer.setPixelRatio( window.devicePixelRatio )
    renderer.setSize( window.innerWidth, window.innerHeight )
    container.appendChild( renderer.domElement )

    controls = new THREE.OrbitControls( camera, renderer.domElement )
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.screenSpacePanning = false;
    controls.minDistance = 100;
    controls.maxDistance = 500;
    controls.maxPolarAngle = Math.PI;

    let radius = 100
    let height = radius / 2
    let geometry = new THREE.CylinderBufferGeometry(radius, radius, height, 32, 1)
    // material = new THREE.MeshPhongMaterial( { color: new THREE.Color( 0.25, 0.45, 0.89), flatShading: true } )
    // material = new THREE.MeshBasicMaterial( {color: new THREE.Color( 0.75, 0.45, 0.21)} );
    for(let i=0 ; i<6 ; i++) {
        let material = new THREE.MeshPhongMaterial( { color: new THREE.Color(Math.random(), Math.random(), Math.random()), emissive: 0x072534, side: THREE.DoubleSide, flatShading: true, transparent: true, opacity: 0.5 } )
        materials.push(material)
    }

    let mesh = new THREE.Mesh( geometry, materials[0] )

    // group.add( mesh )
    // scene.add( group )


    var lights = [];
    lights[ 0 ] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[ 1 ] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[ 2 ] = new THREE.PointLight( 0xffffff, 1, 0 );

    let distance = 100
    lights[ 0 ].position.set( 0, 2*distance, 0 );
    lights[ 1 ].position.set( distance, 2*distance, distance );
    lights[ 2 ].position.set( - distance, - 2*distance, - distance );

    scene.add( lights[ 0 ] );
    scene.add( lights[ 1 ] );
    scene.add( lights[ 2 ] );
    
    // var axesHelper = new THREE.AxesHelper( 50 );
    // scene.add( axesHelper );

    renderer.clear()

}

function randomMaterial() {
    return materials[Math.floor(Math.random()*materials.length)]
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize( window.innerWidth, window.innerHeight )
}

function animate() {
    requestAnimationFrame( animate )
    controls.update()
    render()
}


function render() {
    renderer.render( scene, camera )
}

let resetGUI = (gui)=> {
    let nControllers = gui.__controllers.length
    for(let i=2 ; i<nControllers ; i++) {
        gui.remove(gui.__controllers[2])
    }
}

let main = ()=> {

    var canvas = document.getElementById('canvas')
    paper.setup(canvas)

    initialize()


    window.addEventListener("resize", onWindowResize, false )


    var gui = new dat.GUI({hideable: true})
    window.gui = gui

    let generatorNames = []

    for(let [key, value] of generators) {
        generatorNames.push(key)
    }


    gui.add(parameters, 'generator').options(generatorNames).onChange((value)=> {
        let generator = generators.get(value)
        if(generator) {
            resetGUI(gui)
            generator.initializeGUI(gui)
            generator.generate(null, scene, group)
        }
    })
    gui.add(parameters, 'exportSVG')

    generators.get(parameters.generator).initializeGUI(gui)
    generators.get(parameters.generator).generate()
    
    animate()
}

document.addEventListener("DOMContentLoaded", main)




